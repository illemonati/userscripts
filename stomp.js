// ==UserScript==
// @name         Stomp
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        *://*/*
// @grant        none
// ==/UserScript==


const removeListenersFromElement = (element, type) => {
    element.addEventListener(type, (e) => {
        e.stopImmediatePropagation();
        e.preventDefault();
        e.stopPropagation();
        console.log(`${type} event triggered`);
        return false;
    }, true)
}




const sleep = (ms) => new Promise(r => setTimeout(r, ms));

const main = async () => {
    console.log(window.frames);
    //while (true) {
        removeListenersFromElement(window, 'blur');
        removeListenersFromElement(window, 'focus');
        removeListenersFromElement(window, 'visibilitychange');
        document.hasFocus = () => true;
    //    await sleep(200);
    //}
}


main().then();
