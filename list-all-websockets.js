// ==UserScript==
// @name         ListAllWebsockets
// @namespace    http://tampermonkey.net/
// @version      0.1
// @author       illemonati
// @description  lists all websockets in window.wss
// @grant        none
// @match        *://*/*
// ==/UserScript==

(function() {
    'use strict';
    window.wss = [];
    const ws = window.WebSocket;
    window.WebSocket = function(...args) {
        const socket = new ws(...args);
        const id = Object.keys(window.wss).length;
        window.wss[id] = {
            id,
            socket,
            messages: [],
            sent: [],
        };
        socket.addEventListener('message', (msg) => window.wss[id].messages.push(msg));
        const ogSend = socket.send;
        socket.send = function(...args) {
            ogSend.apply(this, [...args]);
            window.wss[id].sent.push([...args]);
        }
        console.log(socket);
        return socket;
    }
    window.WebSocket.prototype = ws.prototype;
    Object.assign(window.WebSocket, ws);

})();