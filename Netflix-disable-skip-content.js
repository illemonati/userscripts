// ==UserScript==
// @name         Netflix disable skip content
// @version      0.1
// @description  remove netflix skip content bar
// @author       illemonati
// @match        https://www.netflix.com/watch/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    const getSkipContentBar = () => document.querySelector('.watch-video--skip-content');
    const removeSkipContentBar = () => getSkipContentBar().remove();
    window.setInterval(removeSkipContentBar, 500);
})();
