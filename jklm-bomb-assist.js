// ==UserScript==
// @name         jklm bomb assist
// @namespace    https://tong.icu
// @version      0.1
// @description  helper for jklm bomb game
// @author       illemonati
// @run-at       document-start
// @match        https://*.jklm.fun/*
// @exclude      https://jklm.fun/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=jklm.fun
// @grant        none
// ==/UserScript==

(async function() {
    'use strict';
    console.log('activating assist');
    const sleep = async (ms) => await (new Promise(r => setTimeout(r, ms)));
    const allLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    const playerData = {id: 0, bonusLetters: {}};

    for (let letter of allLetters) {
        playerData.bonusLetters[letter] = 0;
    }



    const wsProxy = new Proxy(window.WebSocket, {

        construct: function(target, args) {
            console.log('making ws');
            const ws = new target(...args);
            console.log(ws);

            ws.addEventListener('message', (e) => {
                const msg = e.data;
                console.log(msg);
                if (!msg) return;

                if (msg.startsWith('42["correctWord"')) {
                    const data = JSON.parse(msg.match(/^\d+(.*)$/)[1]);
                    const dataPlayerId = data[1]['playerPeerId'];
                    console.log(playerData.id, dataPlayerId);
                    if (dataPlayerId !== playerData.id) return;
                    const newBonusLetters = data[1]['bonusLetters'];
                    for (let [k, v] of Object.entries(newBonusLetters)) {
                        playerData.bonusLetters[k.toUpperCase()] = v;
                    }
                    console.log(playerData.bonusLetters);

                } else if (msg.startsWith('42["setup"')) {
                    const data = JSON.parse(msg.match(/^\d+(.*)$/)[1]);
                    playerData.id = data[1]['selfPeerId'];
                    console.log('set player id: ',playerData.id)
                }


            });
            return ws;
        }
    });

    window.WebSocket = wsProxy;







    const numSuggestions = 20;


    const wordListURL = 'https://raw.githubusercontent.com/dolph/dictionary/master/popular.txt';
    const wordListRes = await fetch(wordListURL);
    const wordListRaw = await wordListRes.text();
    const wordList = wordListRaw.split('\n').map(word => word.toUpperCase());

    console.log(wordList);


    const suggestionsDiv = document.createElement('div');
    document.body.appendChild(suggestionsDiv);
    suggestionsDiv.style.position = 'absolute';
    suggestionsDiv.style.top = '50%';
    suggestionsDiv.style.left = '2rem';
    suggestionsDiv.style.transform = 'translateY(-50%)';
    suggestionsDiv.style.color = 'white';
    suggestionsDiv.style.fontFamily = 'monospace, monospace';

    const wordLabel = document.createElement('h3');
    suggestionsDiv.appendChild(wordLabel);
    wordLabel.innerText = 'Waiting for Start';
    wordLabel.style.fontWeight = 'bold';
    wordLabel.style.marginBottom = '2rem';

    const wordsDiv = document.createElement('div');
    suggestionsDiv.appendChild(wordsDiv);
    wordsDiv.style.display = 'flex';
    wordsDiv.style.flexDirection = 'column';

    const getNumBonusLettersInWord = (word) => {

        const bonus = {...playerData.bonusLetters};

        let amount = 0;
        for (let letter of word) {
            if (letter in bonus) {
                if (bonus[letter] > 0) {
                    amount ++;
                }
                bonus[letter] --;
            }
        }

        return amount;
    }



    const generateSuggestions = (syllable) => {
        console.log(playerData.bonusLetters);
        const eligibleWords = wordList.filter(word => word.includes(syllable));
        //const sortedWords = eligibleWords.sort((a, b) => a.length - b.length);

        const wordsWithRanking = eligibleWords.map(word => ([word, getNumBonusLettersInWord(word)]));

        console.log(wordsWithRanking);

        const sortedRankingWords = wordsWithRanking.sort((a, b) => {
            return b[1] - a[1];
        });

        const sortedLengthWords = sortedRankingWords.sort((a, b) => {
            const res = b[1] - a[1];
            if (res !== 0) return res;
            return a[0].length - b[0].length;
        });


        console.log(sortedLengthWords);
        const truncatedWords = sortedLengthWords.slice(0, numSuggestions);
        return truncatedWords;
    };

    const updateSuggestions = (syllable) => {
        console.log(syllable);
        wordLabel.innerText = syllable;
        const suggestionWords = generateSuggestions(syllable);

        console.log(suggestionWords);


        wordsDiv.innerHTML = '';

        for (let word of suggestionWords) {
            const p = document.createElement('p');
            wordsDiv.appendChild(p);
            p.innerText = `${word[1].toString().padEnd(2)} ${word[0]}`;
            p.style.marginBottom = '0.2rem';
        }



    };

    const attach = () => {
        console.log('waiting for start');
        const syllableDiv = document.querySelector('.syllable');
        if (!syllableDiv) {
            return;
        }

        console.log('started', syllableDiv);

        const observer = new MutationObserver((mutations) => {
            mutations.forEach((mutation) => {
                updateSuggestions(syllableDiv.innerText);
            });
        });

        observer.observe(syllableDiv, {
            characterData: true,
            attributes: true,
            childList: true,
            subtree: true
        });


        return true;



    }


    while (true) {
        if (attach()) break;
        await sleep(500);
    };
})();
