// ==UserScript==
// @name         discord
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://discord.com/*
// @grant        none
// ==/UserScript==

(function() {
    document.head.innerHTML +=
        `
        <style>
        @import url('https://fonts.googleapis.com/css2?family=Inconsolata:wght@200;300;400;500;600;700;800;900&display=swap');

        * {
            font-family: Inconsolata,Arial,sans-serif !important;
        }

        .theme-dark.theme-dark {
            --text-link: #268bd2;
            --background-primary: #002b36;
            --background-secondary: #003847;
            --background-secondary-alt: #00212b;
            --background-tertiary: #002b36;
            --channeltextarea-background: var(--background-secondary);
        }
        </style>
        `.trim();
})();