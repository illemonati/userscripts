// ==UserScript==
// @name         AutoDiscordWelcome
// @version      0.1
// @author       illemonati
// @match        https://discord.com/channels/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    const all = () => {
        const getWelcomeButtons = () => Array.from(document.querySelectorAll('.welcomeCTAButton-24ZR8d'));
        const welcomeButtons = getWelcomeButtons();
        const updateWelcomeButtons = () => getWelcomeButtons()
        .filter(b => !welcomeButtons.includes(b))
        .forEach(b => {
            welcomeButtons.push(b);
            b.click();
        })
        setInterval(updateWelcomeButtons, 1000);
    }
    setTimeout(all, 30000);
})();
