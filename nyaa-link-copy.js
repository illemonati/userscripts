// ==UserScript==
// @name         Nyaa Link Copy
// @match        https://nyaa.si/?*
// @match        https://nyaa.si/
// @match        https://sukebei.nyaa.si/?*
// @match        https://sukebei.nyaa.si/
// @version      0.1
// @description  Copy Links From Nyaa.si
// @author       illemonati
// ==/UserScript==

(function () {
    "use strict";

    class NyaaLinkCopyHandler {
        db = {};
        selected = new Set();
        selectStarted = false;

        insertHTML() {
            const box = `
                <div class="nyaa-link-copy-box">
                    <button class="nyaa-link-copy-start-select">Start Select</button>
                    <button class="nyaa-link-copy-views">Copy Views</button>
                    <button class="nyaa-link-copy-downloads">Copy Downloads</button>
                    <button class="nyaa-link-copy-magnets">Copy Magnets</button>
                    <button class="nyaa-link-copy-clear">Clear</button>
                </div>
            `;
            const style = `
                <style type="text/css">
                    .nyaa-link-copy-box {
                        width: auto;
                        height: auto;
                        display: flex;
                        flex-direction: column;
                        gap: 0.5em;
                        position: fixed;
                        left: 0.3em;
                        top: calc(100vh / 2);
                        transform: translateY(-50%);
                    }
                    .nyaa-link-copy-selected {
                        background: #A7C7E7 !important;
                    }
                    .nyaa-link-copy-selected > * {
                        background: #A7C7E7 !important;
                    }
                </style>
            `;
            document.body.innerHTML += box + style;
        }

        buildJSONDB() {
            this.db = {};
            const table = document.querySelector("table.torrent-list");
            const tbody = table.querySelector("tbody");
            const rows = tbody.querySelectorAll("tr");
            for (let [i, row] of rows.entries()) {
                const rowData = {};
                const links = Array.from(row.querySelectorAll("a")).map(
                    (a) => a.href
                );
                rowData["download"] = links.filter((l) =>
                    /.*[nyaa.si]?\/download\/+/.test(l)
                )[0];
                rowData["view"] = links.filter((l) =>
                    /.*[nyaa.si]?\/view\/+/.test(l)
                )[0];
                rowData["magnet"] = links.filter((l) =>
                    l.startsWith("magnet:")
                )[0];
                rowData["row"] = row;
                row.classList.add(`nyaa-link-copy-row-${i}`);
                this.db[i] = rowData;
            }
        }

        select(rowNum) {
            const row = this.db[rowNum];
            if (this.selected.has(rowNum)) {
                this.selected.delete(rowNum);
                row.row.classList.remove(`nyaa-link-copy-selected`);
            } else {
                this.selected.add(rowNum);
                row.row.classList.add(`nyaa-link-copy-selected`);
            }
        }

        startSelect = () => {
            if (this.selectStarted) return;
            this.selectStarted = true;
            Object.entries(this.db).forEach(([i, row]) => {
                row["listener"] = () => this.select(i);
                row.row.addEventListener("click", row.listener);
            });
        };

        clear() {
            this.selected.forEach((rowNum) => {
                this.db[rowNum].row.classList.remove(`nyaa-link-copy-selected`);
            });
            this.selected.clear();
            Object.entries(this.db).forEach(([i, row]) => {
                row.row.removeEventListener("click", row.listener);
                row.listener = null;
            });
            this.selectStarted = false;
        }

        copy(catagory) {
            let res = ``;
            this.selected.forEach((rowNum) => {
                res += this.db[rowNum][catagory] + "\n";
            });
            navigator.clipboard.writeText(res);
        }

        start() {
            console.log("Nyaa Link Copy Activated");

            this.insertHTML();
            console.log("HTML inserted");

            this.buildJSONDB();
            console.log("DB built");

            this.selected = new Set();
            document
                .querySelectorAll(".nyaa-link-copy-start-select")
                .forEach((b) =>
                    b.addEventListener("click", () => this.startSelect())
                );

            document
                .querySelectorAll(".nyaa-link-copy-clear")
                .forEach((b) =>
                    b.addEventListener("click", () => this.clear())
                );

            [`download`, `view`, `magnet`].forEach((catagory) => {
                document
                    .querySelectorAll(`.nyaa-link-copy-${catagory}s`)
                    .forEach((b) =>
                        b.addEventListener("click", () => this.copy(catagory))
                    );
            });
        }
    }

    const handler = new NyaaLinkCopyHandler();
    handler.start();
})();
